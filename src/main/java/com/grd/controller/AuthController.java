package com.grd.controller;

import com.grd.model.LoginReq;
import com.grd.security.JwtTokenProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "帳號權限相關操作")
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @ApiOperation(value = "登入")
    @PostMapping(value = "/login")
    public ResponseEntity login(@RequestBody  LoginReq loginReq) {
        String jwt = jwtTokenProvider.generateToken(loginReq.getUsername());
        return ResponseEntity.ok(jwt);
    }
}
