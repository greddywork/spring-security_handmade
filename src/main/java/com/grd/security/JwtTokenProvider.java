package com.grd.security;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Calendar;
import java.util.Date;

@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);
    private static final int JWT_EXPIRATION_MS = 216000;
    private static final String JWT_SECRET = "secret";

    public String generateToken(String username) {
        return tokenBuilder(username);
    }

    private String tokenBuilder(String username) {
        Date now = Calendar.getInstance().getTime();
        Date expiryDate = new Date(now.getTime() + JWT_EXPIRATION_MS);
        return Jwts.builder()
                   .setIssuer(ServletUriComponentsBuilder.fromCurrentContextPath()
                                                         .build()
                                                         .toUriString())
                   .setSubject(username)
                   .setIssuedAt(now)
                   .setExpiration(expiryDate)
                   .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                   .compact();
    }

    public String getUsernameFromJWT(String token) {
        try {
            logger.info("get token:{}", token);
            Claims claims = getClaimsFromToken(token);
            return claims.getSubject();
        } catch (ExpiredJwtException ex) {
            return ex.getClaims().getSubject();
        }
    }

    private Claims getClaimsFromToken(String token) {
        return Jwts.parser()
                   .setSigningKey(JWT_SECRET)
                   .parseClaimsJws(token)
                   .getBody();
    }

    public boolean validateToken(String authToken) {
        try {
            getClaimsFromToken(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
            throw ex;
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
            throw ex;
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
            throw ex;
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
            throw ex;
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
            throw ex;
        }
    }
}
