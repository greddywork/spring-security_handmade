package com.grd.controller;

import com.grd.security.CurrentUser;
import com.grd.security.UserPrincipal;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = "使用者相關操作")
@RestController
@RequestMapping("/user")
public class UserController {

    @ApiOperation(value = "使用者資訊")
    @GetMapping(value = "/info")
    public ResponseEntity info(@ApiIgnore @CurrentUser UserPrincipal currentUser) {
        return ResponseEntity.ok(currentUser.getUsername());
    }
}
